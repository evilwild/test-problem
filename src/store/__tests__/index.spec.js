import {
  mutations, actions, getters, state,
} from '../index';

describe('Username store', () => {
  describe('state', () => {
    Storage.prototype.getItem = jest.fn();
    const storageMock = Storage.prototype.getItem.mock;
    it('should call getItem', () => {
      // eslint-disable-next-line
      state().username;
      expect(storageMock.calls[0][0]).toBe('username');
    });
    it('should be empty string', () => {
      expect(state().username).toBe('');
    });
  });
  describe('mutations', () => {
    const localState = () => ({ username: 'TestMutationsUser' });
    it('should call SET_USERNAME mutation', () => {
      const { SET_USERNAME } = mutations;
      SET_USERNAME(localState, 'TestMutationsUser2');
      expect(localState.username).toBe('TestMutationsUser2');
    });
    it('should do LOGOUT mutation', () => {
      const { LOGOUT } = mutations;
      LOGOUT(localState);
      expect(localState.username).toBe('');
    });
  });
  describe('actions', () => {
    const localState = () => ({
      username: 'TestActionsUser',
    });
    const commit = jest.fn();
    describe('setUsername', () => {
      const { setUsername } = actions;
      it('should not do mutation due string length', () => {
        setUsername({ commit, localState }, '');
        expect(localState().username).toBe('TestActionsUser');
      });
      it('should not do mutation due not string', () => {
        setUsername({ commit, localState }, 5);
        expect(localState().username).toBe('TestActionsUser');
      });
      it('should do mutation', () => {
        setUsername({ commit, localState }, 'User');
        expect(commit.mock.calls.length).toBe(1);
      });
    });
    describe('logout', () => {
      const { logout } = actions;
      it('should call LOGOUT mutation', () => {
        logout({ commit, localState });
        expect(commit.mock.calls[1][0]).toBe('LOGOUT');
      });
    });
  });
  describe('getters', () => {
    const localState = () => ({ username: 'TestGettersUser' });
    it('should get username', () => {
      const { getUsername } = getters;
      const getterValue = getUsername(localState());
      expect(getterValue).toBe('TestGettersUser');
    });
  });
});
