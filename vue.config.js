module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
        @import '@/styles/colors.scss';
        @import '@/styles/mixins.scss';
        @import '@/styles/variables.scss';`,
      },
    },
  },
};
